package Principal;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import Combo.Combo;
import Lanche.Lanche;

public class Principal {

	public Principal(){
		System.out.println("Menu de op��es: ");
		List<String> listacombo = new ArrayList<String>();
		List<String> listalanche = new ArrayList<String>();
		Spike c = new Spike();
		try {
			listacombo = c.carregaCombo();
			listalanche = c.carregaLanche();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		int k = 1;
		if(!listalanche.isEmpty())
		for (int i = 0; i < listalanche.size(); i++) {
			System.out.println(k+" - "+listalanche.get(i));
			k++;
		}
		if(!listacombo.isEmpty())
		for (int i = 0; i < listacombo.size(); i++) {
			System.out.println(k+" - "+listacombo.get(i));
			k++;
		}
		System.out.println("Selecione a op��o:");
		Scanner s = new Scanner(System.in);
		int op = s.nextInt();
		if(op <= listalanche.size()){
			op--;
			defineDecoratorLanche(listalanche.get(op), c, s);
		}else{
			op = op - listalanche.size();
			Class klass;
			try {
				klass = Class.forName("Combo."+listacombo.get(op));
				Combo ob = (Combo) klass.newInstance();
				ArrayList<String> aux = ob.getCombo();
				for (int i = 0; i < aux.size(); i++) {
					defineDecoratorLanche(aux.get(i), c, s);
				}
			} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	private void defineDecoratorLanche(String nome, Spike spike, Scanner s){
		Class klass, klassdec;
		try {
			System.out.println("Sub menu do tipo de "+nome);
			List<String> lista = null;
			Method [] met = Spike.class.getMethods();
			for (int i = 0; i < met.length; i++) {
				//System.out.println(met[i].getName());
				if(met[i].getName().contains(nome)){
					//System.out.println("ola");
					lista = (List<String>) met[i].invoke(spike, new Object[0]);
				}
			}
			if(!lista.isEmpty())
			for (int i = 0; i < lista.size(); i++) {
				System.out.println((i+1)+" - "+lista.get(i));
			}
			int op = s.nextInt();
			System.out.println(lista.get(op-1));
			klassdec = Class.forName("Decorator"+nome+"."+lista.get(op-1));
			klass = Class.forName("Lanche."+nome);
			Constructor<Lanche> c = klassdec.getConstructor(Lanche.class);
			Lanche lanche = (Lanche) c.newInstance((Lanche) klass.newInstance());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}

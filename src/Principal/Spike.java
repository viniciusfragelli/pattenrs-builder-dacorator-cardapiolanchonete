package Principal;

import java.io.File;
import java.util.ArrayList;

import Combo.Combo;
import Lanche.Lanche;

public class Spike {
	
	public ArrayList<String> carregaCombo() throws InstantiationException, IllegalAccessException, ClassNotFoundException{
		ArrayList<String> aux = new ArrayList<>();
		File dir = new File("bin\\Combo");
		for(File f : dir.listFiles()){
			if(f.getName().endsWith(".class")){
				String name=f.getName().substring(0,f.getName().length()-6);
				//Class klass = Class.forName("Combo."+name);
				//if(!klass.getSimpleName().equals("Combo"))
				if(!name.equalsIgnoreCase("Combo"))
				{
					aux.add(name);
				}
			}
		}
		return aux;
	}
	
	public ArrayList<String> carregaLanche() throws InstantiationException, IllegalAccessException, ClassNotFoundException{
		ArrayList<String> aux = new ArrayList<>();
		File dir = new File("bin\\Lanche");
		for(File f : dir.listFiles()){
			if(f.getName().endsWith(".class")){
				String name=f.getName().substring(0,f.getName().length()-6);
				//Class klass = Class.forName("Lanche."+name);
				//if(!klass.getSimpleName().equals("Lanche"))
				if(!name.equalsIgnoreCase("Lanche"))
				{
					aux.add(name);
				}
			}
		}
		return aux;
	}
	
	public static ArrayList<String> carregaDecoratorBatata() throws InstantiationException, IllegalAccessException, ClassNotFoundException{
		ArrayList<String> aux = new ArrayList<>();
		File dir = new File("bin\\DecoratorBatata");
		for(File f : dir.listFiles()){
			if(f.getName().endsWith(".class")){
				String name=f.getName().substring(0,f.getName().length()-6);
				aux.add(name);
			}
		}
		return aux;
	}
	
	public static ArrayList<String> carregaDecoratorMilkShake() throws InstantiationException, IllegalAccessException, ClassNotFoundException{
		ArrayList<String> aux = new ArrayList<>();
		File dir = new File("bin\\DecoratorMilkShake");
		for(File f : dir.listFiles()){
			if(f.getName().endsWith(".class")){
				String name=f.getName().substring(0,f.getName().length()-6);
				aux.add(name);
			}
		}
		return aux;
	}
	
	public static ArrayList<String> carregaDecoratorXBurguer() throws InstantiationException, IllegalAccessException, ClassNotFoundException{
		ArrayList<String> aux = new ArrayList<>();
		File dir = new File("bin\\DecoratorXBurguer");
		for(File f : dir.listFiles()){
			if(f.getName().endsWith(".class")){
				String name=f.getName().substring(0,f.getName().length()-6);
				aux.add(name);
			}
		}
		return aux;
	}
}

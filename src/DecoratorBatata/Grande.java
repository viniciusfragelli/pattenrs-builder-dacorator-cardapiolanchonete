package DecoratorBatata;

import Lanche.Batata;
import Lanche.Lanche;

public class Grande extends Lanche{

	private Lanche l;
	
	public Grande(Lanche l){
		this.l = new Batata();
		montar();
	}
	
	@Override
	public void montar() {
		System.out.println("Montar batata grande com queijo");
		l.montar();
	}

	@Override
	public String getNome() {
		return "Grande - 400g";
	}

}

package DecoratorBatata;

import Lanche.Lanche;

public class Pequena extends Lanche{

	private Lanche l;
	
	public Pequena(Lanche l){
		this.l = l;
		montar();
	}
	
	@Override
	public void montar() {
		System.out.println("Montando batata pequena!");
		l.montar();
	}

	@Override
	public String getNome() {
		return "Pequena - 200g";
	}

}

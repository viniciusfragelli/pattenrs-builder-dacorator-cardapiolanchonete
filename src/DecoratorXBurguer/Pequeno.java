package DecoratorXBurguer;

import Lanche.Lanche;

public class Pequeno extends Lanche{

	private Lanche l;
	
	public Pequeno(Lanche l) {
		this.l = l;
		montar();
	}
	
	@Override
	public void montar() {
		System.out.println("Sanduiche pequeno");
		l.montar();
	}

	@Override
	public String getNome() {
		// TODO Auto-generated method stub
		return "Pequeno";
	}

}

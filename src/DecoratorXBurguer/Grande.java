package DecoratorXBurguer;

import Lanche.Lanche;

public class Grande extends Lanche{

	private Lanche l;
	
	public Grande(Lanche l) {
		this.l = l;
		montar();
	}
	
	@Override
	public void montar() {
		System.out.println("Sanduiche grande com maionese catchup e mostarda");
		l.montar();
	}

	@Override
	public String getNome() {
		// TODO Auto-generated method stub
		return "Grande";
	}

}

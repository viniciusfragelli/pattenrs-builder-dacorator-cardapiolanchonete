package DecoratorMilkShake;

import Lanche.Lanche;

public class SemRaspas extends Lanche{

	private Lanche l;
	
	public SemRaspas(Lanche l){
		this.l = l;
		montar();
	}

	@Override
	public void montar() {
		l.montar();
		
	}

	@Override
	public String getNome() {
		return "Sem Raspas";
	}
	
}

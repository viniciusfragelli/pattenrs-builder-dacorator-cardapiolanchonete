package DecoratorMilkShake;

import Lanche.Lanche;

public class ComRaspas extends Lanche{

	private Lanche l;
	
	public ComRaspas(Lanche l){
		this.l = l;
		montar();
	}

	@Override
	public void montar() {
		System.out.println("Adicionando Raspas");
		l.montar();
		
	}

	@Override
	public String getNome() {
		return "Com Raspas";
	}
	
}

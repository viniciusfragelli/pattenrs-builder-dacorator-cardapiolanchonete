package Lanche;

public class XBurguer extends Lanche{

	public XBurguer(){
		//montar();
	}
	
	@Override
	public void montar() {
		carne();
		moldarHamburguer();
		fritarHamburguer();
		pegarPao();
		cortarPao();
		colocarQueijo();
		colocarHamburguer();
	}

	private void carne(){
		System.out.println("Pesando carne");
	}
	
	private void moldarHamburguer(){
		System.out.println("Moldando hamburguer");
	}
	
	private void fritarHamburguer(){
		System.out.println("Fritando hambuerguer");
	}
	
	private void pegarPao(){
		System.out.println("Pegando p�o");
	}
	
	private void cortarPao(){
		System.out.println("Cortando p�o");
	}
	
	private void colocarQueijo(){
		System.out.println("Colocando queijo");
	}
	
	private void colocarHamburguer(){
		System.out.println("Colocando hamburguer");
	}

	@Override
	public String getNome() {
		
		return "XBurguer";
	}
	
}

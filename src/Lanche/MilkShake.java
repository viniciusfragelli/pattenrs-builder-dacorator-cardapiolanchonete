package Lanche;

public class MilkShake extends Lanche{

	public MilkShake(){
		//montar();
	}
	
	@Override
	public void montar() {
		adicionarLeite();
		adicionarSorvete();
	}
	
	private void adicionarLeite(){
		System.out.println("Adicionando leite");
	}

	private void adicionarSorvete(){
		System.out.println("Adicionando sorvete");
	}
	
	@Override
	public String getNome() {
		return "MilkShake";
	}

}

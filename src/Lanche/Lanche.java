package Lanche;

public abstract class Lanche {
	
	protected String nome;
	
	public abstract void montar();
	
	public abstract String getNome();
	
}

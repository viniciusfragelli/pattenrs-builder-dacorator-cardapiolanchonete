package Combo;

import java.util.ArrayList;
import java.util.List;

import Lanche.*;

public class Combo1 extends Combo{
	
	public Combo1(){
		componentesDoCombo();
	}
	
	@Override
	public void componentesDoCombo() {
		nome = "Combo 1";
		comp = new ArrayList<String>();
		comp.add("XBurguer");
		comp.add("MilkShake");
	}

	public String getNome(){
		return nome;
	}
	
	public void setNome(String nome){
		this.nome = nome;
	}

	@Override
	public ArrayList<String> getCombo() {
		return (ArrayList<String>) comp;
	}
	
	
}

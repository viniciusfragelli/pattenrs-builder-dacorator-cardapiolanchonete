package Combo;

import java.util.ArrayList;
import java.util.List;

import Lanche.Lanche;

public abstract class Combo {
	
	protected String nome;
	protected List<String> comp;
	
	public abstract void componentesDoCombo();
	
	public abstract String getNome();
	
	public abstract ArrayList<String> getCombo();
	
}

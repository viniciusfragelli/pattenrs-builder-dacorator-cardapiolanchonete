package Combo;

import java.util.ArrayList;

import Lanche.*;

public class Combo2 extends Combo{

	public Combo2(){
		componentesDoCombo();
	}
	
	@Override

	public String getNome(){
		return nome;
	}
	
	public void setNome(String nome){
		this.nome = nome;
	}

	@Override
	public void componentesDoCombo() {
		nome = "Combo 2";
		comp = new ArrayList<String>();
		comp.add("XBurguer");
		comp.add("MilkShake");
		comp.add("Batata");
	}

	@Override
	public ArrayList<String> getCombo() {
		return (ArrayList<String>) comp;
	}

}
